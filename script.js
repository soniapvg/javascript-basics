"use strict";

// ====================================================================
// Exercise 1
// ====================================================================

let showGreetings = () => {
  let greetings = "Bonjour";
  console.log( greetings += " " + prompt("Ton prénom ?") );
}

// ====================================================================
// Exercise 2
// ====================================================================

let showFactorial = () => {
  let pickedNumber = parseInt(prompt("Un nombre ?"), 10);
  console.log(`${pickedNumber}! = ${computeFactorialOf(pickedNumber)}`);
}

function computeFactorialOf(number) {
  if ( isNaN(number) || number < 0 ) {
    return 'no result -> must be a positive number';
  } else if (number == 0) {
    return 1;
  } else {
    return number *= computeFactorialOf(number - 1);
  }
}

// ====================================================================
// Exercise 3
// ====================================================================

let showPyramid = () => {
  let pickedFloorNum = parseInt(prompt("Combien d'étages ?"), 10);
  console.log(`${buildPyramid(pickedFloorNum)}`);
}

function buildPyramid(floorNum) {
  let pyramid = [];

  for (let i = 1; i <= floorNum; i++) {
    let floor = '&nbsp;'.repeat(floorNum - i) + '#'.repeat(i) ;
    pyramid.push(floor);
  }

  return pyramid.join('<br>');
}

// ====================================================================
// Exercise 4
// ====================================================================

let businessPeople = [
  { first: 'Steve', last: 'Jobs', year: 1955 },
  { first: 'Oprah', last: 'Winfrey', year: 1954 },
  { first: 'Bill', last: 'Gates', year: 1955 },
  { first: 'Sheryl', last: 'Sandberg', year: 1969 },
  { first: 'Mark', last: 'Zuckerberg', year: 1984 },
  { first: 'Beyonce', last: 'Knowles', year: 1981 },
  { first: 'Jeff', last: 'Bezos', year: 1964 },
  { first: 'Diane', last: 'Hendricks', year: 1947 },
  { first: 'Elon', last: 'Musk', year: 1971 },
  { first: 'Marissa', last: 'Mayer', year: 1975 },
  { first: 'Walt', last: 'Disney', year: 1901 },
  { first: 'Larry', last: 'Page', year: 1973 },
  { first: 'Jack', last: 'Dorsey', year: 1976 },
  { first: 'Evan', last: 'Spiegel', year: 1990 },
  { first: 'Brian', last: 'Chesky', year: 1981 },
  { first: 'Travis', last: 'Kalanick', year: 1976 },
  { first: 'Marc', last: 'Andreessen', year: 1971 },
  { first: 'Peter', last: 'Thiel', year: 1967 }
];

let findBornSeventies = () => {
  let peopleBornSeventies = businessPeople.filter(person => (person.year >= 1970 && person.year < 1980 ));

  console.log('filtered object (born in the 70s):');
  console.log(peopleBornSeventies);
}

let createNamesArray = () => {
  let peopleNames= businessPeople.map(person => `${person.first} ${person.last}`);

  console.log('first and last name array:');
  console.log(peopleNames);
}

let computeAgeToday = () => {
  let now = new Date();
  let peopleAges = businessPeople.map(person => ({
    "full name": `${person.first} ${person.last}`,
    "today's age": now.getFullYear() - person.year
  }));

  console.log("new array of objects with name and today's age:");
  console.log(peopleAges);
}

let sortByFamilyName = () => {
  businessPeople.sort( (a, b) => {
    if (a.last < b.last) return -1;
    if (a.last > b.last) return 1;
    return 0;
  });

  console.log('new array sorted by name:');
  console.log(businessPeople);
}

// ====================================================================
// Exercise 5
// ====================================================================

let books = [
  { title: 'Gatsby le magnifique', id: 133712, rented: 39 },
  { title: 'A la recherche du temps perdu', id: 237634, rented: 28 },
  { title: 'Orgueil & Préjugés', id: 873495, rented: 67 },
  { title: 'Les frères Karamazov', id: 450911, rented: 55 },
  { title: 'Dans les forêts de Sibérie', id: 8376365, rented: 15 },
  { title: 'Pourquoi j\'ai mangé mon père', id: 450911, rented: 45 },
  { title: 'Et on tuera tous les affreux', id: 67565, rented: 36 },
  { title: 'Le meilleur des mondes', id: 88847, rented: 58 },
  { title: 'La disparition', id: 364445, rented: 33 },
  { title: 'La lune seule le sait', id: 63541, rented: 43 },
  { title: 'Voyage au centre de la Terre', id: 4656388, rented: 38 },
  { title: 'Guerre et Paix', id: 748147, rented: 19 }
];

let wereBorrowedOnce = () => {
  let bool = 'yes';
  books.forEach(book => {
    if (book.rented == 0) return bool = 'no';
  });
  console.log('were all books borrowed at least once?');
  console.log(bool);
}

let findMostBorrowed = () => {
  books.sort( (a, b) => {
    if (a.rented < b.rented) return -1;
    if (a.rented > b.tented) return 1;
    return 0;
  });

  console.log('most borrowed:');
  console.log(books[books.length - 1].title);
}

let findLeastBorrowed = () => {
  books.sort( (a, b) => {
    if (a.rented < b.rented) return -1;
    if (a.rented > b.tented) return 1;
    return 0;
  });

  console.log('least borrowed:');
  console.log(books[0].title);
}

let findById = () => {
  let book = books.find(book => book.id == 873495);

  console.log('id 873495:');
  console.log(book.title);
}

let deleteById = () => {
  let index = books.findIndex(book => book.id == 133712);
  books.splice(index, 1);

  console.log('array without id 133712:');
  console.log(books);
}

let sortByTitle = () => {
  books.sort( (a, b) => {
    if (a.title < b.title) return -1;
    if (a.title > b.title) return 1;
    return 0;
  });

  console.log('array of sorted books:');
  console.log(books);
}

// ====================================================================
// Exercise 6
// ====================================================================

let showARNTranslations = () => {
  console.log(translateToProtein('CCGUCGUUGCGCUACAGC'));
  console.log(translateToProtein('CCUCGCCGGUACUUCUCG'));
}

function translateToProtein(arn) {
  arn = arn.match(/.{3}/g);
  let proteins = arn.map(codon => {
    switch (true) {
      case /UCU|UCC|UCA|UCG|AGU|AGC/.test(codon): return 'Sérine';
      case /CCU|CCC|CCA|CCG/.test(codon): return 'Proline';
      case /UUA|UUG/.test(codon): return 'Leucine';
      case /UUU|UUC/.test(codon): return 'Phenylalanine';
      case /CGU|CGC|CGA|CGG|AGA|AGG/.test(codon): return 'Arginine';
      case /UAU|UAC/.test(codon): return 'Tyrosine';
    }
  });
  return proteins.join('-');
}

// ====================================================================
// Exercise 7
// ====================================================================

function showBotAnswer() {
  let input = prompt("Parle avec le bot :");
  switch (true) {
    case /\?$/.test(input):
      console.log('Ouais Ouais...');
      break;
    case /^[A-Z\s]+$/.test(input):
      console.log('Pwa, calme-toi...');
      break;
    case /.*fortnite.*/.test(input):
      console.log("on s' fait une partie soum-soum ?");
      break;
    case input.length == 0:
      console.log("t'es en PLS ?");
      break;
    default:
      console.log('balek.');
  }
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// CONSOLE IN HTML
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

console.log = (message) => {

  if (typeof message == 'object') message = JSON.stringify(message);

  document.querySelector('.console-logs-list').insertAdjacentHTML(
    'beforeend',
    `<li class="console-logs-item">${message}</li>`
  );
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// BUTTON ACTIONS
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Console
document.querySelector('.console-btn--reset').addEventListener('click', () => {
  document.location.reload();
});

// Exercises
const allExercises = [
  showGreetings,
  showFactorial,
  showPyramid,
  findBornSeventies,
  createNamesArray,
  computeAgeToday,
  sortByFamilyName,
  wereBorrowedOnce,
  findMostBorrowed,
  findLeastBorrowed,
  findById,
  deleteById,
  sortByTitle,
  showARNTranslations,
  showBotAnswer
];

document.querySelectorAll('.exercise-btn').forEach( (button, index) => {
  button.addEventListener('click', allExercises[index]);
});